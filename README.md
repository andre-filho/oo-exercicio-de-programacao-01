Exercício de programação 01 -- Programação Orientada a Objetos
Autor: André se Sousa Costa Filho

-----------------------------------------------------------------

OBS.: Este programa foi desenvolvido usando Ubuntu 14.04, logo utilize alguma distribuição Linux para a execução.

Como gerar o executável do programa:

1- com o terminal escolha uma pasta de sua preferência e use
		$ git clone https://gitlab.com/andre.filho/oo-exercicio-de-programacao-01.git

2- Após a clonagem do repositório, abra a pasta clonada.

3- confira a existencia das imagens a serem decodificadas na pasta "img" e na subpasta do formato.

4- NÃO ALTERE O NOME DAS IMAGENS SEM ANOTAR! Caso aconteça de mudar a imagem, lembre-se do nome da mesma quando for solicitado.

5- No teminal, entre no diretório do repositório e digite:

			$ make

e por fim execute o programa:

			$ make run

6- Siga as instruções do programa.

7- A saída pgm será em formato de texto (.txt) e estará na mesma pasta do makefile. A saída da aplicação de filtro na imagem ppm saira no formato ppm (.ppm) tambem na pasta onde se encontra o makefile

8- O executavel se encontra na pasta bin, com o nome de "decoder", geralmente não é necessário mexer nele.
