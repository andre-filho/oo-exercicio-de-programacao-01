#ifndef IMAGEM_H
#define IMAGEM_H

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

// classe abstrata
  class Imagem{

  public:
    //  Métodos
    int getAltura();
    int getLargura();
    int getValorMaxCor();
    string getNumeroMagico();
    char* getCaminhoImagem();


    void setLargura(ifstream &arquivo);
    void setAltura(ifstream &arquivo);
    void setValorMaxCor(ifstream &arquivo);
    void setNumeroMagico(ifstream &arquivo);
    void setCaminhoImagem();
    void setCabecalho(ifstream &arquivo);

  private:
    // Atributos
    int largura;
    int altura;
    int valor_max_cor;
    string numero_magico;
    char caminho_imagem[256];

};

#endif
