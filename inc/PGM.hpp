#ifndef PGM_H
#define PGM_H

#include <iostream>
#include <fstream>
#include "Imagem.hpp"
using namespace std;

class PGM : public Imagem{

public:
  // Métodos
  PGM();
  void decodificarPGM(ifstream &arquivo);
  void decodificarLENA(ifstream &arquivo);

private:
  // Atributos
  char bitExtraido;
  char byteArquivo;
  char caracter;

};

#endif
