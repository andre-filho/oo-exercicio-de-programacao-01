#ifndef PPM_H
#define PPM_H

#include "Imagem.hpp"
#include <iostream>
#include <fstream>
using namespace std;

class PPM : public Imagem {

public:
// métodos
  PPM();
  PPM(fstream &arquivo);
  void lerPPM(ifstream &arquivo, PPM * ppm);
  void escreverPPM(PPM *ppm);
  void escolhaFiltro(PPM *ppm);
  void filtroAzul(PPM *ppm);
  void filtroVerde(PPM *ppm);
  void filtroVermelho(PPM *ppm);
  void executarPPM(PPM *ppm);

  struct Rgb{
    char r,g,b;
    Rgb(): r(0), g(0), b(0) {}
    Rgb(char _r,char _g, char _b) : r(_r), g(_g), b(_b){}
  };
  Rgb *pixel;
};

#endif
