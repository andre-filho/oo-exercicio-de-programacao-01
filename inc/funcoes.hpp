#ifndef FUNCOES_H
#define FUNCOES_H

#include <iostream>
#include <cctype>
#include "Imagem.hpp"
using namespace std;

void checkObjectAlloc(Imagem* op);

void deleteObjects(Imagem* op1, Imagem* op2);

char menuInicial();

#endif
