#include <limits>
#include "Imagem.hpp"
using namespace std;

//setters
void Imagem::setAltura(ifstream &arquivo){
  arquivo >> altura;
}

void Imagem::setLargura(ifstream &arquivo){
  arquivo >> largura;
}

void Imagem::setNumeroMagico(ifstream &arquivo){

  string num;
  while(arquivo >> num)
  {
    if(num == "#")
    {
      arquivo.ignore(numeric_limits<char>::max(), '\n');
      break;
    }
    numero_magico = num;
  }
}

void Imagem::setCaminhoImagem(){
  cout << "Digite o caminho da imagem (exemplo: img/pgm_ou_ppm/nome_da_imagem.pgm): ";
  cin.get(caminho_imagem, 256);
}

void Imagem::setCabecalho(ifstream &arquivo){
  char c;
     arquivo >> numero_magico;
     while(arquivo.get(c)){
       if(c == '#'){
         arquivo.ignore(numeric_limits<char>::max(),'\n');
         break;
       }
     }

    arquivo >> largura;
    arquivo >> altura;
    arquivo >> valor_max_cor;

}

void Imagem::setValorMaxCor(ifstream &arquivo){
  arquivo >> valor_max_cor;
}

// getters
int Imagem::getAltura(){
  return altura;
}

int Imagem::getLargura(){
  return largura;
}

string Imagem::getNumeroMagico(){
  return numero_magico;
}

char* Imagem::getCaminhoImagem(){
  return caminho_imagem;
}

int Imagem::getValorMaxCor(){
  return valor_max_cor;
}
