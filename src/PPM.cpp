#include "PPM.hpp"
#include <iostream>
#include <fstream>
#include <cstdlib>
using namespace std;

PPM::PPM(){
}

void PPM::lerPPM(ifstream &arquivo, PPM *ppm){
  ppm->pixel = new PPM::Rgb[ppm->getLargura() * ppm->getAltura()];
  char pix[3];
  for(int i=0;i< ppm->getLargura() * ppm->getAltura();i++){
    arquivo.read(reinterpret_cast<char *>(pix), 3);
    ppm->pixel[i].r = pix[0];
    ppm->pixel[i].g = pix[1];
    ppm->pixel[i].b = pix[2];
  }
}

void PPM::escreverPPM(PPM *ppm){
  ofstream file("img/ppm/resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = ppm->pixel[i].r;
    g = ppm->pixel[i].g;
    b = ppm->pixel[i].b;
    file << r << g << b;
  }
  file.close();
}

void PPM::filtroAzul(PPM *ppm){
  ofstream file("resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 0;
    g = 0;
    b = 255-ppm->pixel[i].b;
    file << r << g << b;
  }
  file.close();
}

void PPM::filtroVerde(PPM *ppm){
  ofstream file("resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r = 0;
    g = 255-ppm->pixel[i].g;
    b = 0;
    file << r << g << b;
  }
  file.close();
}

void PPM::filtroVermelho(PPM *ppm){
  ofstream file("resultado.ppm",ofstream::binary);
  if(!file){
    cerr << "Não foi possível abrir o arquivo" << endl;
    exit(1);
   }

  file << "P6\n";
  file << ppm->getLargura()<< " " << ppm->getAltura();
  file << "\n255\n";

  char r,g,b;
  for(int i=0; i < ppm->getLargura() * ppm->getAltura(); i++){
    r =255-ppm->pixel[i].r;
    g = 0;
    b = 0;
    file << r << g << b;
  }
  file.close();

}

void PPM::escolhaFiltro(PPM *ppm){
  int opcao;
  cout << "Selecione o filtro: " << endl;
  cout << "1.Filtro vermelho"<<endl;
  cout << "2.Filtro verde"<<endl;
  cout << "3.Filtro azul" <<endl;
  cin >> opcao;

  setbuf(stdin, NULL);

  switch (opcao) {
    case 1: return ppm->filtroVermelho(ppm);
            break;
    case 2: return ppm->filtroVerde(ppm);
            break;
    case 3: return ppm->filtroAzul(ppm);
            break;
  }
}

void PPM::executarPPM(PPM *ppm){
  ppm->setCaminhoImagem();
  ifstream file (ppm->getCaminhoImagem(),ifstream :: in | ifstream::binary );
   if(!file){
     cerr << "Não foi possível abrir o arquivo" << endl;
     exit(1);
   }
   ppm->setCabecalho(file);
   ppm->lerPPM(file,ppm);
   ppm->escolhaFiltro(ppm);

   file.close();
}
