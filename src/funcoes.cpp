#include "funcoes.hpp"
using namespace std;

void checkObjectAlloc(Imagem* op)
{
  if(op == NULL){
    cout << "Ocorreu um problema na alocação de memória para o objeto da imagem.";
    cout << "\nAperte qualque tecla para terminar a execução..."<< endl;
    }
}

void deleteObjects(Imagem* op1, Imagem* op2)
{
  delete(op1);
  delete(op2);
}

char menuInicial()
{
  char op;

  cout << "Olá, seja bem vindo ao decodificador." << endl;
  cout << "Por favor, escolha o formato de imagem que deseja decodificar: \na- PGM\nb- PPM" << endl;

  cin >> op;
  op = tolower(op);
  return op;
}
