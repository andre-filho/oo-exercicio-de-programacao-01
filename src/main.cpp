#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include "Imagem.hpp"
#include "PGM.hpp"
#include "PPM.hpp"
#include "funcoes.hpp"
using namespace std;


int main(int argc, char ** argv){

  char op;
  string nome;

  PGM * imagem_pgm = new PGM();
  PPM * imagem_ppm = new PPM();

  checkObjectAlloc(imagem_pgm);
  checkObjectAlloc(imagem_ppm);

  ifstream file;
  fstream pgmdata;

  op = menuInicial();

  switch(op)
  {
    case 'a':

    setbuf(stdin, NULL);
    imagem_pgm->setCaminhoImagem();

    nome = imagem_pgm->getCaminhoImagem();
    cout << endl;

    file.open(imagem_pgm->getCaminhoImagem(), ifstream::in | ifstream::binary);

    pgmdata.open("pgmdata.txt", fstream::out);

    if(!file || !pgmdata)
    {
      cerr << "Os arquivos não puderam ser abertos." << endl;
      cerr << "Verifique se os mesmos se encontram no caminho: /img/pgm/" << endl;
      return 0;
    }

    imagem_pgm->setNumeroMagico(file);
    imagem_pgm->setAltura(file);
    imagem_pgm->setLargura(file);
    imagem_pgm->setValorMaxCor(file);

    pgmdata << "Dados imagem pgm:" << '\n';
    pgmdata << imagem_pgm->getNumeroMagico() << " -> número mágico" << '\n';
    pgmdata << imagem_pgm->getAltura() << " x ";
    pgmdata << imagem_pgm->getLargura() << " -> altura x largura" << '\n';
    pgmdata << imagem_pgm->getValorMaxCor() << " -> valor max de cor";

    if(nome == "img/pgm/lena.pgm")
    {
      imagem_pgm->decodificarLENA(file);  // por conta da pos inicial
    }
    imagem_pgm->decodificarPGM(file);

    cout << endl;

    file.close();
    pgmdata.close();

    break;

    case'b':

    setbuf(stdin, NULL);

    imagem_ppm->executarPPM(imagem_ppm);
    cout << "aplicação do filtro concluida com sucesso!" << endl;
    break;

    default:
    cout << "Opção não válida!" << endl;
    break;

  }

  deleteObjects(imagem_pgm, imagem_ppm);

  return 0;
}
